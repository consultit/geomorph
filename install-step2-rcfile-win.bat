REM Installing a Geomorph user, part 2:  defaults for the options file

@echo off

set VERSION=0.62

set HOME=%HOMEDRIVE%%HOMEPATH%
REM Double \\ for PovRAY parsing
set HOME=%HOME:\=\\%
set GEORC="%HOME%\\geomorphrc"
set IEXPLORE=C:\\Program Files\\Internet Explorer\\iexplore.exe
set FIREFOX=C:\\Program Files\\Mozilla Firefox\\firefox.exe

IF EXIST %GEORC% GOTO :EOF

echo [files] > %GEORC%
echo def_dir = %HOME%\\geomorph\\ ; 		>> %GEORC%
echo hf_dir = %HOME%\\geomorph\\ ; 		>> %GEORC%
echo pov_dir = %HOME%\\geomorph\\ ; 		>> %GEORC%
echo tmp_dir = %HOME%\\geomorph\\tmp\\ ; 		>> %GEORC%
echo doc_dir = sourceforge.geomorph.net ;	>> %GEORC%

REM Look for a HTML reader 

IF EXIST "%FIREFOX%" GOTO :FIREFOX

IF EXIST "%IEXPLORE%" GOTO :IEXPLORE

IEXPLORE:
	echo doc_reader =  %IEXPLORE% ;		>> %GEORC%
	GOTO :INTERFACE

REM Firefox and Iexplorer not found

	echo doc_reader =  ;			>> %GEORC%
	echo Please check your doc_reader variable.
	echo It should be your browser, ex. C:\\Program Files\\Mozilla Firefox\\firefox.exe
	GOTO :INTERFACE

:FIREFOX
	echo doc_reader =  %FIREFOX% ;		>> %GEORC%

:INTERFACE

echo [interface] >> %GEORC%
echo interface_style      = Integrated ;	>> %GEORC%
echo menu_in_doc_window   = TRUE ;		>> %GEORC%
echo icons_in_doc_window  = TRUE ;		>> %GEORC%
echo pad = 6 ;					>> %GEORC%
echo hf_size_in_screen_percent = 40 ;		>> %GEORC%
echo hf_size_in_pixels    = 512 ; 		>> %GEORC%
echo doc_display_size     = 512 ;		>> %GEORC%
echo max_pen_preview      = 64  ;		>> %GEORC%
echo filter_preview_size  = 32  ;		>> %GEORC%
echo noise_preview_size   = 64  ;		>> %GEORC%
echo main_bar_x           = 3   ;		>> %GEORC%
echo main_bar_y           = 3   ;		>> %GEORC%
echo creation_window_x    = 3   ;		>> %GEORC%
echo creation_window_y    = 15  ;		>> %GEORC%
echo tools_window_x       = 3   ;		>> %GEORC%
echo tools_window_y       = 15  ;		>> %GEORC%
echo display_doc_offset   = 4   ;		>> %GEORC%

echo [application] >> %GEORC%
echo max_history          = 5  ;		>> %GEORC%
echo default_terrain      = Subdiv2 ;		>> %GEORC%
echo default_seed         = 118670938 ;		>> %GEORC%

echo [rendering] >> %GEORC%

REM Look for povray

dir /s/b "C:\Program Files" | findstr /i /r /c:"pvengine.*exe" /c:"povray.*exe" > povlist.txt

REM Count lines

set file=povlist.txt
set /a cnt=0
for /f %%a in ('type "%file%"^|find "" /v /c') do set /a cnt=%%a
echo %cnt% candidate names found for POV-Ray in %file%
type %file%

REM Try to get only 1 line
IF %cnt% GTR 0 GOTO :SELECTSSE2

REM Nothing found
echo POV-Ray not found, please check where it is and assign the program name
echo with its full path in the renderer= variable of geomorphrc
echo renderer = ;			>> %GEORC%

GOTO :ENDPOVRAY

:SELECTSSE2

REM We first try the sse2 version (most x86 CPUs have these extensions since 2003)

set file=povlist2.txt

findstr /i /c:"sse2" povlist.txt > %file%
set /a cnt=0
for /f %%a in ('type povlist2.txt^|find "" /v /c') do set /a cnt=%%a
echo %cnt% candidate names found for POV-Ray in %file%
type %file%

IF %cnt% EQU 0 set file=povlist.txt

REM We assign the name of the last executable found to the renderer variable

for /f "delims=" %%a in (%file%) do set "RENDERER=%%a"
echo renderer = %RENDERER% ;		>> %GEORC%

:ENDPOVRAY

IF EXIST povlist.txt DEL povlist.txt
IF EXIST povlist2.txt DEL povlist2.txt

echo render_width         =  640 ;	>> %GEORC%
echo render_height        =  480 ;	>> %GEORC%
echo scene = simple_terrain.pov ; >> %GEORC%
echo hf_output_for_rendering = test.png ; >> %GEORC%
echo output_prefix        = _    ;	>> %GEORC%
echo other_render_options = +P +D -F ;	>> %GEORC%


